package com.mitocode.beans;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.mitocode.interfaces.IEquipo;
@Component
public class Barcelona implements IEquipo{

	public String mostrar() {
		
		return"Barcelona FC";
	}

}
